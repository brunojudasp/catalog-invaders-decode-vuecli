import { shallowMount } from '@vue/test-utils'
import App from '@/App'

describe('App', () => {
  const build = () => {
    const wrapper = shallowMount(App, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    return {
      wrapper
    }
  }

  it('renders the component', () => {
    // arrange
    const { wrapper } = build()
    // assert
    expect(wrapper.html()).toMatchSnapshot()
  })
})
