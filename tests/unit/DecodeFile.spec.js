import { shallowMount } from '@vue/test-utils'
import DecodeFile from '@/components/DecodeFile'

describe('DecodeFile', () => {
  it('test', () => {
    expect(true).toBe(true)
  })

  it('renders the component', () => {
    const wrapper = shallowMount(DecodeFile, {
      global: {
        mocks: {
          $t: (msg) => msg
        }
      }
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
